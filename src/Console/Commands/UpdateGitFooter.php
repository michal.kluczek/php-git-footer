<?php

namespace Wocozon\GitFooter\Console\Commands;

class UpdateGitFooter extends \Illuminate\Console\Command
{
    protected $signature = 'wocozon:git-footer';

    protected $description = 'Update Git information displayed on website\'s footer';

    public function handle()
    {
        $path    = config('git-footer.file_path');

        $branch  = $_ENV['CI_COMMIT_BRANCH'] ?? $this->shell(['git', 'symbolic-ref', '--short', 'HEAD']);
        $commit  = $this->shell(['git', 'rev-parse', '--short', 'HEAD']);
        $env     = config('app.env');
        $tag     = $this->shell(['git', 'describe', '--tags']);

        file_put_contents($path, sprintf("[branch: $branch] [commit: $commit] [env: $env] [tag: $tag]"));
    }

    private function shell(array $command): string
    {
        $process = new \Symfony\Component\Process\Process($command, base_path());

        $process->run();

        if (!$process->isSuccessful()) {
            return '-unknown-';
        }

        return trim(str_replace("\n", '', $process->getOutput()));
    }
}
